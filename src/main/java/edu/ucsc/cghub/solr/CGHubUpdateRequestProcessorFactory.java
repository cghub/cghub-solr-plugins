package edu.ucsc.cghub.solr;

import java.io.IOException;

import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.update.processor.UpdateRequestProcessor;
import org.apache.solr.update.processor.UpdateRequestProcessorFactory;

import java.util.regex.Pattern;
import java.util.regex.Matcher;


public class CGHubUpdateRequestProcessorFactory extends UpdateRequestProcessorFactory
{
	@Override
	public UpdateRequestProcessor getInstance(SolrQueryRequest req, SolrQueryResponse rsp, UpdateRequestProcessor next)
	{
		return new CGHubUpdateRequestProcessor(next);
	}
}

class CGHubUpdateRequestProcessor extends UpdateRequestProcessor
{
	public CGHubUpdateRequestProcessor( UpdateRequestProcessor next) {
		super( next );
	}

	private static Pattern pattern = Pattern.compile(
		"(?s)" +
		"<ANALYSIS_ATTRIBUTE>\\s*" +
			"<TAG>\\s*PREVIOUS_REASONS\\s*</TAG>\\s*" +
			"<VALUE>\\s*(\\{.*?\\})\\s*</VALUE>\\s*" +
		"</ANALYSIS_ATTRIBUTE>" );

	@Override
	public void processAdd(AddUpdateCommand cmd) throws IOException {
		SolrInputDocument doc = cmd.getSolrInputDocument();
		Object v = doc.getFieldValue( "analysis_xml" );
		if( v != null ) {
			Matcher m = pattern.matcher( v.toString() );
			while( m.find() ) {
				doc.addField( "previous_reasons", m.group( 1 ) );
			}
		}
		// pass it up the chain
		super.processAdd( cmd );
	}
}
